﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guest : MonoBehaviour
{
    private Transform _busyChair;



    private void Awake()
    {
        EventsSystem.AddEvent( SimpleEvent.ChooseChair, ChooseChair );
    }

    private void Update()
    {
        if ( !_busyChair ) return;

        transform.position = Vector3.MoveTowards( transform.position, _busyChair.position, Time.deltaTime * 10 );
    }

    private void ChooseChair()
    {
        if ( _busyChair && Random.Range( 1, 101 ) > 45 ) return;
        _busyChair = Game.GetFreeChair( _busyChair );
    }
}
