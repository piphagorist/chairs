﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private GameObject _chairOrigin;
    [SerializeField] private GameObject _guestOrigin;

    [SerializeField] private Spawner _spawner;


    public static GameObject ChairOrigin;
    public static GameObject GuestOrigin;


    private Transform[] _chairs;

    private static List<Transform> _freeChairs;



    private void Awake()
    {
        EventsSystem.InitEvents();

        ChairOrigin = _chairOrigin;
        GuestOrigin = _guestOrigin;

        _spawner.Init( out _chairs );

        _freeChairs = new List<Transform>( _chairs );
    }

    private void Update()
    {
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            EventsSystem.CallEvent( SimpleEvent.ChooseChair );
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }


    public static Transform GetFreeChair( Transform vacatedChair )
    {
        if ( vacatedChair )
            _freeChairs.Add( vacatedChair );

        Transform chair = _freeChairs[ Random.Range( 0, _freeChairs.Count ) ];
        _freeChairs.Remove( chair );

        return chair;
    }
}
