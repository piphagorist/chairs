﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Spawner : MonoBehaviour
{
    public void Init( out Transform[] chairs )
    {
        chairs = SpawnObjects( 10, Game.ChairOrigin, 4 );
        SpawnObjects( 6, Game.GuestOrigin, 2 );
    }


    private Transform[] SpawnObjects( int quantity, GameObject origin, float distance )
    {
        Transform[] objects = new Transform[ quantity ];

        for ( int i = 0; i < quantity; i++ )
        {
            float angle = ( 360.0f / quantity ) * ( i + 1.0f );
            float xPos = Mathf.Sin( angle * Mathf.Deg2Rad );
            float yPos = Mathf.Cos( angle * Mathf.Deg2Rad );

            Vector2 position = new Vector2( xPos, yPos ) * distance;

            objects[ i ] = Instantiate( origin ).transform;
            objects[ i ].position = position;
        }

        return objects;
    }
}
